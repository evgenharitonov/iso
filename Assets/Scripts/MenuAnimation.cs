﻿using UnityEngine;

public class MenuAnimation : MonoBehaviour {

	public GameObject MenuPanel;

	public void OpenMenu()
	{
		if (MenuPanel != null)
		{
			Animator animator = MenuPanel.GetComponent<Animator>();
			if (animator != null)
			{
				bool isOpen = animator.GetBool("open");

				animator.SetBool("open", !isOpen);
			}
		}
	}
}
