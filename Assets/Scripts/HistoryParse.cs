﻿using System.Collections;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

public class HistoryParse : MonoBehaviour
{

	public string[] graphData;
	public string WWWLoadManager;
	public string tablename;
	public Toggle store;
	public TMP_Text tabledata, tabledata2, tabledata3, period, period2, period3;
	public Text tabledata4, tabledata5, tabledata6, tabledata7, tabledata8, tabledata9, tabledata10, tabledata11, tabledata12, tabledata13, tabledata14, tabledata15, tabledata16, tabledata17, tabledata18, tabledata19, tabledata20, tabledata21, tabledata22, tabledata23, tabledata24;
	public Dropdown dropdown;
	void Start()
	{
		StartCoroutine(ParseData());
	}

	IEnumerator ParseData()
	{
		WWWForm table = new WWWForm();
		table.AddField("tablename", tablename);
		WWW dataDB = new WWW(WWWLoadManager, table);
		yield return dataDB;
		string msg = dataDB.text;
		graphData = msg.Split('~');
		int month = dropdown.value + 1;
		if (month == 1)
		{
			period.text = "Январь";
			period2.text = "Январь";
			period3.text = "Январь";
		}
		if (month == 2)
		{
			period.text = "Февраль";
			period2.text = "Февраль";
			period3.text = "Февраль";
		}
		if (month == 3)
		{
			period.text = "Март";
			period2.text = "Март";
			period3.text = "Март";
		}
		if (month == 4)
		{
			period.text = "Апрель";
			period2.text = "Апрель";
			period3.text = "Апрель";
		}
		if (month == 5)
		{
			period.text = "Май";
			period2.text = "Май";
			period3.text = "Май";
		}
		if (month == 6)
		{
			period.text = "Июнь";
			period2.text = "Июнь";
			period3.text = "Июнь";
		}
		if (month == 7)
		{
			period.text = "Июль";
			period2.text = "Июль";
			period3.text = "Июль";
		}
		if (month == 8)
		{
			period.text = "Август";
			period2.text = "Август";
			period3.text = "Август";
		}
		if (month == 9)
		{
			period.text = "Сентябрь";
			period2.text = "Сентябрь";
			period3.text = "Сентябрь";
		}
		if (month == 10)
		{
			period.text = "Октябрь";
			period2.text = "Октябрь";
			period3.text = "Октябрь";
		}
		if (month == 11)
		{
			period.text = "Ноябрь"; 
			period2.text = "Ноябрь";
			period3.text = "Ноябрь";
		}
		if (month == 12)
		{
			period.text = "Декабрь";
			period2.text = "Декабрь";
			period3.text = "Декабрь";
		}


		tabledata.text = GetDataValue(graphData[0], "t0:");
		tabledata2.text = GetDataValue(graphData[8], "t0:");
		tabledata3.text = GetDataValue(graphData[16], "t0:");
		if (String.IsNullOrEmpty(GetDataValue(graphData[1], $"t{month}:")) == false)
		{
			float tmp1 = Mathf.Round((float.Parse(GetDataValue(graphData[1], $"t{month}:"))) / 1000);
			tabledata4.text = tmp1.ToString();
		}
		else
		{
			tabledata4.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[2], $"t{month}:")) == false)
		{
			float tmp2 = Mathf.Round((float.Parse(GetDataValue(graphData[2], $"t{month}:"))) / 1000);
			tabledata5.text = tmp2.ToString();
		}
		else
		{
			tabledata5.text = "-";
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[3], $"t{month}:")) == false)
		{
			float tmp3 = Mathf.Round((float.Parse(GetDataValue(graphData[3], $"t{month}:"))) / 1000);
			tabledata6.text = tmp3.ToString();
		}
		else
		{
			tabledata6.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{month}:")) == false)
		{
			float tmp4 = Mathf.Round((float.Parse(GetDataValue(graphData[4], $"t{month}:"))) / 1000);
			tabledata7.text = tmp4.ToString();
		}
		else
		{
			tabledata7.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[5], $"t{month}:")) == false)
		{
			float tmp5 = Mathf.Round((float.Parse(GetDataValue(graphData[5], $"t{month}:"))) / 1000);
			tabledata8.text = tmp5.ToString();
		}
		else
		{
			tabledata8.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[6], $"t{month}:")) == false)
		{
			float tmp6 = Mathf.Round((float.Parse(GetDataValue(graphData[6], $"t{month}:"))) / 1000);
			tabledata9.text = tmp6.ToString();
		}
		else
		{
			tabledata9.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[7], $"t{month}:")) == false)
		{
			float tmp6_ = Mathf.Round((float.Parse(GetDataValue(graphData[7], $"t{month}:"))) / 1000);
			tabledata10.text = tmp6_.ToString();
		}
		else
		{
			tabledata10.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{month}:")) == false)
		{
			float tmp7 = Mathf.Round((float.Parse(GetDataValue(graphData[9], $"t{month}:"))) / 1000);
			tabledata11.text = tmp7.ToString();
		}
		else
		{
			tabledata11.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[10], $"t{month}:")) == false)
		{
			float tmp8 = Mathf.Round((float.Parse(GetDataValue(graphData[10], $"t{month}:"))) / 1000);
			tabledata12.text = tmp8.ToString();
		}
		else
		{
			tabledata12.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[11], $"t{month}:")) == false)
		{
			float tmp9 = Mathf.Round((float.Parse(GetDataValue(graphData[11], $"t{month}:"))) / 1000);
			tabledata13.text = tmp9.ToString();
		}
		else
		{
			tabledata13.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[12], $"t{month}:")) == false)
		{
			float tmp10 = Mathf.Round((float.Parse(GetDataValue(graphData[12], $"t{month}:"))) / 1000);
			tabledata14.text = tmp10.ToString();
		}
		else
		{
			tabledata14.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[13], $"t{month}:")) == false)
		{
			float tmp11 = Mathf.Round((float.Parse(GetDataValue(graphData[13], $"t{month}:"))) / 1000);
			tabledata15.text = tmp11.ToString();
		}
		else
		{
			tabledata15.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{month}:")) == false)
		{
			float tmp12 = Mathf.Round((float.Parse(GetDataValue(graphData[14], $"t{month}:"))) / 1000);
			tabledata16.text = tmp12.ToString();
		}
		else
		{
			tabledata16.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[15], $"t{month}:")) == false)
		{
			float tmp13 = Mathf.Round((float.Parse(GetDataValue(graphData[15], $"t{month}:"))) / 1000);
			tabledata17.text = tmp13.ToString();
		}
		else
		{
			tabledata17.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[17], $"t{month}:")) == false)
		{
			float tmp14 = Mathf.Round((float.Parse(GetDataValue(graphData[17], $"t{month}:"))) / 1000);
			tabledata18.text = tmp14.ToString();
		}
		else
		{
			tabledata18.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[18], $"t{month}:")) == false)
		{
			float tmp15 = Mathf.Round((float.Parse(GetDataValue(graphData[18], $"t{month}:"))) / 1000);
			tabledata19.text = tmp15.ToString();
		}
		else
		{
			tabledata19.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[19], $"t{month}:")) == false)
		{
			float tmp16 = Mathf.Round((float.Parse(GetDataValue(graphData[19], $"t{month}:"))) / 1000);
			tabledata20.text = tmp16.ToString();
		}
		else
		{
			tabledata20.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[20], $"t{month}:")) == false)
		{
			float tmp17 = Mathf.Round((float.Parse(GetDataValue(graphData[20], $"t{month}:"))) / 1000);
			tabledata21.text = tmp17.ToString();
		}
		else
		{
			tabledata21.text = "-"; ;
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[21], $"t{month}:")) == false)
		{
			float tmp18 = Mathf.Round((float.Parse(GetDataValue(graphData[21], $"t{month}:"))) / 1000);
			tabledata22.text = tmp18.ToString();
		}
		else
		{
			tabledata22.text = "-";
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[22], $"t{month}:")) == false)
		{
			float tmp19 = Mathf.Round((float.Parse(GetDataValue(graphData[22], $"t{month}:"))) / 1000);
			tabledata23.text = tmp19.ToString();
		}
		else
		{
			tabledata23.text = "-";

		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[23], $"t{month}:")) == false)
		{
			float tmp20 = Mathf.Round((float.Parse(GetDataValue(graphData[23], $"t{month}:"))) / 1000);
			tabledata24.text = tmp20.ToString();
		}
		else
		{
			tabledata24.text = "-";

		}

	}

	public void MonthUpdater()
	{
		int month = dropdown.value + 1;
		if (store.isOn)
		{
			if (month == 1)
			{
				period.text = "Январь";
				period2.text = "Январь";
				period3.text = "Январь";
			}
			if (month == 2)
			{
				period.text = "Январь - Февраль";
				period2.text = "Январь - Февраль";
				period3.text = "Январь - Февраль";
			}
			if (month == 3)
			{
				period.text = "Январь - Март";
				period2.text = "Январь - Март";
				period3.text = "Январь - Март";
			}
			if (month == 4)
			{
				period.text = "Январь - Апрель";
				period2.text = "Январь - Апрель";
				period3.text = "Январь - Апрель";
			}
			if (month == 5)
			{
				period.text = "Январь - Май";
				period2.text = "Январь - Май";
				period3.text = "Январь - Май";
			}
			if (month == 6)
			{
				period.text = "Январь - Июнь";
				period2.text = "Январь - Июнь";
				period3.text = "Январь - Июнь";
			}
			if (month == 7)
			{
				period.text = "Январь - Июль";
				period2.text = "Январь - Июль";
				period3.text = "Январь - Июль";
			}
			if (month == 8)
			{
				period.text = "Январь - Август";
				period2.text = "Январь - Август";
				period3.text = "Январь - Август";
			}
			if (month == 9)
			{
				period.text = "Январь - Сентябрь";
				period2.text = "Январь - Сентябрь";
				period3.text = "Январь - Сентябрь";
			}
			if (month == 10)
			{
				period.text = "Январь - Октябрь";
				period2.text = "Январь - Октябрь";
				period3.text = "Январь - Октябрь";
			}
			if (month == 11)
			{
				period.text = "Январь - Ноябрь";
				period2.text = "Январь - Ноябрь";
				period3.text = "Январь - Ноябрь";
			}
			if (month == 12)
			{
				period.text = "Январь - Декабрь";
				period2.text = "Январь - Декабрь";
				period3.text = "Январь - Декабрь";
			}
			float tmp1 = 0;
			float tmp2 = 0;
			float tmp3 = 0;
			float tmp4 = 0;
			float tmp5 = 0;
			float tmp6 = 0;
			float tmp6_ = 0;
			float tmp7 = 0;
			float tmp8 = 0;
			float tmp9 = 0;
			float tmp10 = 0;
			float tmp11 = 0;
			float tmp12 = 0;
			float tmp13 = 0;
			float tmp14 = 0;
			float tmp15 = 0;
			float tmp16 = 0;
			float tmp17 = 0;
			float tmp18 = 0;
			float tmp19 = 0;
			float tmp20 = 0;
			float v1 = 0;
			float v2 = 0;
			float v3 = 0;
			float v4 = 0;
			float v5 = 0;
			float v6 = 0;
			float v7 = 0;

			for (int i = 1; i < month+1; i++)
			{

				if (String.IsNullOrEmpty(GetDataValue(graphData[1], $"t{i}:")) == false)
				{
					tmp1 = Mathf.Round(((float.Parse(GetDataValue(graphData[1], $"t{i}:"))) / 1000));
					v1 = tmp1 + v1;
					tabledata4.text = v1.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[2], $"t{i}:")) == false)
				{
					tmp2 = Mathf.Round(((float.Parse(GetDataValue(graphData[2], $"t{i}:"))) / 1000));
					v2 = tmp2 + v2;
					tabledata5.text = v2.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[3], $"t{i}:")) == false)
				{
					tmp3 = Mathf.Round(((float.Parse(GetDataValue(graphData[3], $"t{i}:"))) / 1000));
					v3 = tmp3 + v3;
					tabledata6.text = v3.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{i}:")) == false)
				{
					tmp4 = Mathf.Round(((float.Parse(GetDataValue(graphData[4], $"t{i}:"))) / 1000));
					v4 = tmp4 + v4;
					tabledata7.text = v4.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[5], $"t{i}:")) == false)
				{
					tmp5 = Mathf.Round(((float.Parse(GetDataValue(graphData[5], $"t{i}:"))) / 1000));
					v5 = tmp5 + v5;
					tabledata8.text = v5.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[6], $"t{i}:")) == false)
				{
					tmp6 = Mathf.Round(((float.Parse(GetDataValue(graphData[6], $"t{i}:"))) / 1000));
					v6 = tmp6 + v6;
					tabledata9.text = v6.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[7], $"t{i}:")) == false)
				{
					tmp6_ = Mathf.Round(((float.Parse(GetDataValue(graphData[7], $"t{i}:"))) / 1000));
					v7 = tmp6_ + v7;
					tabledata10.text = v7.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{i}:")) == false)
				{
					tmp7 = Mathf.Round(((float.Parse(GetDataValue(graphData[9], $"t{i}:"))) / 1000)) + tmp7;
					tabledata11.text = tmp7.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[10], $"t{i}:")) == false)
				{
					tmp8 = Mathf.Round(((float.Parse(GetDataValue(graphData[10], $"t{i}:"))) / 1000)) + tmp8;
					tabledata12.text = tmp8.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[11], $"t{i}:")) == false)
				{
					tmp9 = Mathf.Round(((float.Parse(GetDataValue(graphData[11], $"t{i}:"))) / 1000)) + tmp9;
					tabledata13.text = tmp9.ToString();
				}
				if (String.IsNullOrEmpty(GetDataValue(graphData[12], $"t{i}:")) == false)
				{
					tmp10 = Mathf.Round(((float.Parse(GetDataValue(graphData[12], $"t{i}:"))) / 1000)) + tmp10;
					tabledata14.text = tmp10.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[13], $"t{i}:")) == false)
				{
					tmp11 = Mathf.Round(((float.Parse(GetDataValue(graphData[13], $"t{i}:"))) / 1000)) + tmp11;
					tabledata15.text = tmp11.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{i}:")) == false)
				{
					tmp12 = Mathf.Round(((float.Parse(GetDataValue(graphData[14], $"t{i}:"))) / 1000)) + tmp12;
					tabledata16.text = tmp12.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[15], $"t{i}:")) == false)
				{
					tmp13 = Mathf.Round(((float.Parse(GetDataValue(graphData[15], $"t{i}:"))) / 1000)) + tmp13;
					tabledata17.text = tmp13.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[17], $"t{i}:")) == false)
				{
					tmp14 = Mathf.Round(((float.Parse(GetDataValue(graphData[17], $"t{i}:"))) / 1000)) + tmp14;
					tabledata18.text = tmp14.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[18], $"t{i}:")) == false)
				{
					tmp15 = Mathf.Round(((float.Parse(GetDataValue(graphData[18], $"t{i}:"))) / 1000)) + tmp15;
					tabledata19.text = tmp15.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[19], $"t{i}:")) == false)
				{
					tmp16 = Mathf.Round(((float.Parse(GetDataValue(graphData[19], $"t{i}:"))) / 1000)) + tmp16;
					tabledata20.text = tmp16.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[20], $"t{i}:")) == false)
				{
					tmp17 = Mathf.Round(((float.Parse(GetDataValue(graphData[20], $"t{i}:"))) / 1000)) + tmp17;
					tabledata21.text = tmp17.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[21], $"t{i}:")) == false)
				{
					tmp18 = Mathf.Round(((float.Parse(GetDataValue(graphData[21], $"t{i}:"))) / 1000)) + tmp18;
					tabledata22.text = tmp18.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[22], $"t{i}:")) == false)
				{
					tmp19 = Mathf.Round(((float.Parse(GetDataValue(graphData[22], $"t{i}:"))) / 1000)) + tmp19;
					tabledata23.text = tmp19.ToString();
				}

				if (String.IsNullOrEmpty(GetDataValue(graphData[23], $"t{i}:")) == false)
				{
					tmp20 = Mathf.Round(((float.Parse(GetDataValue(graphData[23], $"t{i}:"))) / 1000)) + tmp20;
					tabledata24.text = tmp20.ToString();
				}
			}
		}
		else
		{
		if (month == 1)
		{
			period.text = "Январь";
			period2.text = "Январь";
			period3.text = "Январь";
		}
		if (month == 2)
		{
			period.text = "Февраль";
			period2.text = "Февраль";
			period3.text = "Февраль";
		}
		if (month == 3)
		{
			period.text = "Март";
			period2.text = "Март";
			period3.text = "Март";
		}
		if (month == 4)
		{
			period.text = "Апрель";
			period2.text = "Апрель";
			period3.text = "Апрель";
		}
		if (month == 5)
		{
			period.text = "Май";
			period2.text = "Май";
			period3.text = "Май";
		}
		if (month == 6)
		{
			period.text = "Июнь";
			period2.text = "Июнь";
			period3.text = "Июнь";
		}
		if (month == 7)
		{
			period.text = "Июль";
			period2.text = "Июль";
			period3.text = "Июль";
		}
		if (month == 8)
		{
			period.text = "Август";
			period2.text = "Август";
			period3.text = "Август";
		}
		if (month == 9)
		{
			period.text = "Сентябрь";
			period2.text = "Сентябрь";
			period3.text = "Сентябрь";
		}
		if (month == 10)
		{
			period.text = "Октябрь";
			period2.text = "Октябрь";
			period3.text = "Октябрь";
		}
		if (month == 11)
		{
			period.text = "Ноябрь"; 
			period2.text = "Ноябрь";
			period3.text = "Ноябрь";
		}
		if (month == 12)
		{
			period.text = "Декабрь";
			period2.text = "Декабрь";
			period3.text = "Декабрь";
		}
			if (String.IsNullOrEmpty(GetDataValue(graphData[1], $"t{month}:")) == false)
			{
				float tmp1 = Mathf.Round((float.Parse(GetDataValue(graphData[1], $"t{month}:"))) / 1000);
				tabledata4.text = tmp1.ToString();
			}
			else
			{
				tabledata4.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[2], $"t{month}:")) == false)
			{
				float tmp2 = Mathf.Round((float.Parse(GetDataValue(graphData[2], $"t{month}:"))) / 1000);
				tabledata5.text = tmp2.ToString();
			}
			else
			{
				tabledata5.text = "-";

			}

			if (String.IsNullOrEmpty(GetDataValue(graphData[3], $"t{month}:")) == false)
			{
				float tmp3 = Mathf.Round((float.Parse(GetDataValue(graphData[3], $"t{month}:"))) / 1000);
				tabledata6.text = tmp3.ToString();
			}
			else
			{
				tabledata6.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{month}:")) == false)
			{
				float tmp4 = Mathf.Round((float.Parse(GetDataValue(graphData[4], $"t{month}:"))) / 1000);
				tabledata7.text = tmp4.ToString();
			}
			else
			{
				tabledata7.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[5], $"t{month}:")) == false)
			{
				float tmp5 = Mathf.Round((float.Parse(GetDataValue(graphData[5], $"t{month}:"))) / 1000);
				tabledata8.text = tmp5.ToString();
			}
			else
			{
				tabledata8.text = "-";

			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[6], $"t{month}:")) == false)
			{
				float tmp6 = Mathf.Round((float.Parse(GetDataValue(graphData[6], $"t{month}:"))) / 1000);
				tabledata9.text = tmp6.ToString();
			}
			else
			{
				tabledata9.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[7], $"t{month}:")) == false)
			{
				float tmp6_ = Mathf.Round((float.Parse(GetDataValue(graphData[7], $"t{month}:"))) / 1000);
				tabledata10.text = tmp6_.ToString();
			}
			else
			{
				tabledata10.text = "-";

			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{month}:")) == false)
			{
				float tmp7 = Mathf.Round((float.Parse(GetDataValue(graphData[9], $"t{month}:"))) / 1000);
				tabledata11.text = tmp7.ToString();
			}
			else
			{
				tabledata11.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[10], $"t{month}:")) == false)
			{
				float tmp8 = Mathf.Round((float.Parse(GetDataValue(graphData[10], $"t{month}:"))) / 1000);
				tabledata12.text = tmp8.ToString();
			}
			else
			{
				tabledata12.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[11], $"t{month}:")) == false)
			{
				float tmp9 = Mathf.Round((float.Parse(GetDataValue(graphData[11], $"t{month}:"))) / 1000);
				tabledata13.text = tmp9.ToString();
			}
			else
			{
				tabledata13.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[12], $"t{month}:")) == false)
			{
				float tmp10 = Mathf.Round((float.Parse(GetDataValue(graphData[12], $"t{month}:"))) / 1000);
				tabledata14.text = tmp10.ToString();
			}
			else
			{
				tabledata14.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[13], $"t{month}:")) == false)
			{
				float tmp11 = Mathf.Round((float.Parse(GetDataValue(graphData[13], $"t{month}:"))) / 1000);
				tabledata15.text = tmp11.ToString();
			}
			else
			{
				tabledata15.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{month}:")) == false)
			{
				float tmp12 = Mathf.Round((float.Parse(GetDataValue(graphData[14], $"t{month}:"))) / 1000);
				tabledata16.text = tmp12.ToString();
			}
			else
			{
				tabledata16.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[15], $"t{month}:")) == false)
			{
				float tmp13 = Mathf.Round((float.Parse(GetDataValue(graphData[15], $"t{month}:"))) / 1000);
				tabledata17.text = tmp13.ToString();
			}
			else
			{
				tabledata17.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[17], $"t{month}:")) == false)
			{
				float tmp14 = Mathf.Round((float.Parse(GetDataValue(graphData[17], $"t{month}:"))) / 1000);
				tabledata18.text = tmp14.ToString();
			}
			else
			{
				tabledata18.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[18], $"t{month}:")) == false)
			{
				float tmp15 = Mathf.Round((float.Parse(GetDataValue(graphData[18], $"t{month}:"))) / 1000);
				tabledata19.text = tmp15.ToString();
			}
			else
			{
				tabledata19.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[19], $"t{month}:")) == false)
			{
				float tmp16 = Mathf.Round((float.Parse(GetDataValue(graphData[19], $"t{month}:"))) / 1000);
				tabledata20.text = tmp16.ToString();
			}
			else
			{
				tabledata20.text = "-";

			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[20], $"t{month}:")) == false)
			{
				float tmp17 = Mathf.Round((float.Parse(GetDataValue(graphData[20], $"t{month}:"))) / 1000);
				tabledata21.text = tmp17.ToString();
			}
			else
			{
				tabledata21.text = "-";
			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[21], $"t{month}:")) == false)
			{
				float tmp18 = Mathf.Round((float.Parse(GetDataValue(graphData[21], $"t{month}:"))) / 1000);
				tabledata22.text = tmp18.ToString();
			}
			else
			{
				tabledata22.text = "-";

			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[22], $"t{month}:")) == false)
			{
				float tmp19 = Mathf.Round((float.Parse(GetDataValue(graphData[22], $"t{month}:"))) / 1000);
				tabledata23.text = tmp19.ToString();
			}
			else
			{
				tabledata23.text = "-";

			}
			if (String.IsNullOrEmpty(GetDataValue(graphData[23], $"t{month}:")) == false)
			{
				float tmp20 = Mathf.Round((float.Parse(GetDataValue(graphData[23], $"t{month}:"))) / 1000);
				tabledata24.text = tmp20.ToString();
			}
			else
			{
				tabledata24.text = "-";

			}
		}
	}

	string GetDataValue(string data, string index)
	{
		string value = data.Substring(data.IndexOf(index, StringComparison.CurrentCulture) + index.Length);
		if (value.Contains("|")) value = value.Remove(value.IndexOf("|", StringComparison.CurrentCulture));
		return value;
	}
}
