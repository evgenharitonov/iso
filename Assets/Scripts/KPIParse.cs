﻿using System.Collections;
using UnityEngine;
using System;
using ChartAndGraph;
using TMPro;
using UnityEngine.UI;

public class KPIParse : MonoBehaviour
{

	public string[] graphData;
	public string WWWLoadManager;
	public string tablename;
	public BarChart chart;
	public TMP_Text tabledata, tabledata2, tabledata3, tabledata4, tabledata5, tabledata6, tabledata7, tabledata8, tabledata9, tabledata10, tabledata11, tabledata12, tabledata13, tabledata14, tabledata15, tabledata16;
	public TMP_Text td17, td18, td19, td20, td21, td22, td23, td24, td25, td26, td27, td28, td29, td30, td31, td32, td33, td34, td35, td36, td37, td38, td39, td40, td41, td42, td43, td44, td45, td46, td48, td49, td50, td51, td52, td53, td54, td55, td56, td57;
	public RawImage ct1, ct2, ct3, ct4, ct5, ct6, ct7, ct8, ct9, ct10, ct11, ct12, ct13, ct14, ct15, ct16, ct17, ct18, ct19, ct20, ct21, ct22, ct23, ct24, ct25, ct26;
	void Start()
	{
		ct1.color = Color.green;
		ct2.color = Color.green;
		ct3.color = Color.green;
		ct4.color = Color.green;
		ct5.color = Color.green;
		ct6.color = Color.green;
		ct7.color = Color.green;
		ct8.color = Color.green;
		ct9.color = Color.green;
		ct10.color = Color.green;
		ct11.color = Color.green;
		ct12.color = Color.green;
		ct13.color = Color.green;
		ct14.color = Color.green;
		ct15.color = Color.green;
		ct16.color = Color.green;
		ct17.color = Color.green;
		ct18.color = Color.green;
		ct19.color = Color.green;
		ct20.color = Color.green;
		ct21.color = Color.green;
		ct22.color = Color.green;
		ct23.color = Color.green;
		ct24.color = Color.green;
		ct25.color = Color.green;
		ct26.color = Color.green;
		StartCoroutine(GraphBuild());
	}

	IEnumerator GraphBuild()
	{
		WWWForm table = new WWWForm();
		table.AddField("tablename", tablename);
		WWW dataDB = new WWW(WWWLoadManager, table);
		yield return dataDB;
		string msg = dataDB.text;
		graphData = msg.Split('~');
		chart.DataSource.StartBatch();
		float spwntimer = UnityEngine.Random.Range(0.0f, 0.93f);
		float textvalue1 = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{1}:")) / 1000);
		float textvalue1_ = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{1}:")) / 1000);
		tabledata5.text = textvalue1_.ToString();
		float textvalue2 = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{2}:")) / 1000);
		float textvalue2_ = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{2}:")) / 1000);
		tabledata6.text = textvalue2_.ToString();
		float tmp1 = (textvalue1 / textvalue2) * 100;
		if (textvalue1 < textvalue2)
		{
			ct1.color = Color.red;
		}
		chart.DataSource.SlideValue("Факт", "Выручка", tmp1, spwntimer);

		float textvalue3 = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{3}:")) / 1000);
		tabledata7.text = textvalue3.ToString();
		float textvalue4 = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{4}:")) / 1000);
		tabledata8.text = textvalue4.ToString();
		float tmp2 = Mathf.Round((textvalue3 / textvalue4) * 100);
		if (textvalue3 < textvalue4)
		{
			ct4.color = Color.red;
		}
		chart.DataSource.SlideValue("Прогноз", "Выручка", tmp2, spwntimer);

		float textvalue5 = Mathf.Round(float.Parse(GetDataValue(graphData[2], $"t{1}:")) / 1000);
		tabledata9.text = textvalue5.ToString();
		float textvalue6 = Mathf.Round(float.Parse(GetDataValue(graphData[2], $"t{2}:")) / 1000);
		tabledata10.text = textvalue6.ToString();
		float tmp5 = Mathf.Round((textvalue5 / textvalue6) * 100);
		if (textvalue5 < textvalue6)
		{
			ct2.color = Color.red; ;
		}
		chart.DataSource.SlideValue("Факт", "Портфель", tmp5, spwntimer);
		float textvalue7 = Mathf.Round(float.Parse(GetDataValue(graphData[2], $"t{3}:")) / 1000);
		tabledata11.text = textvalue7.ToString();
		float textvalue8 = Mathf.Round(float.Parse(GetDataValue(graphData[2], $"t{4}:")) / 1000);
		tabledata12.text = textvalue8.ToString();
		float tmp4 = Mathf.Round((textvalue7 / textvalue8) * 100);
		if (textvalue7 < textvalue8)
		{
			ct5.color = Color.red; ;
		}
		chart.DataSource.SlideValue("Прогноз", "Портфель", tmp4, spwntimer);

		string tv = (GetDataValue(graphData[3], $"t{1}:"));
		tv = tv.Substring(0, tv.Length - 1);
		float textvalue9 = float.Parse(tv);
		tabledata13.text = GetDataValue(graphData[3], $"t{1}:");
		string tv2 = (GetDataValue(graphData[3], $"t{2}:"));
		tv2 = tv2.Substring(0, tv2.Length - 1);
		float textvalue10 = float.Parse(tv2);
		tabledata14.text = GetDataValue(graphData[3], $"t{2}:");
		float tmp6 = (textvalue9 / textvalue10) * 100;
		if (textvalue9 < textvalue10)
		{
			ct3.color = Color.red;
		}
		chart.DataSource.SlideValue("Факт", "ИПНП", tmp6, spwntimer);

		string tv3 = (GetDataValue(graphData[3], $"t{3}:"));
		tv3 = tv3.Substring(0, tv3.Length - 1);
		float textvalue11 = float.Parse(tv3);
		tabledata15.text = GetDataValue(graphData[3], $"t{3}:");
		string tv4 = (GetDataValue(graphData[3], $"t{4}:"));
		tv4 = tv4.Substring(0, tv4.Length - 1);
		float textvalue12 = float.Parse(tv4);
		tabledata16.text = GetDataValue(graphData[3], $"t{4}:");
		float tmp7 = (textvalue11 / textvalue12) * 100;
		if (textvalue11 < textvalue12)
		{
			ct6.color = Color.red;
		}
		chart.DataSource.SlideValue("Прогноз", "ИПНП", tmp7, spwntimer);

		float textvalue13 = Mathf.Round(float.Parse(GetDataValue(graphData[5], $"t{1}:")) / 1000);   //screen2
		td17.text = textvalue13.ToString();
		float textvalue14 = Mathf.Round(float.Parse(GetDataValue(graphData[5], $"t{2}:")) / 1000);
		td18.text = textvalue14.ToString();
		if (textvalue13 < textvalue14)
		{
			ct7.color = Color.red;
		}

		float textvalue15 = Mathf.Round(float.Parse(GetDataValue(graphData[5], $"t{3}:")) / 1000);
		td19.text = textvalue15.ToString();
		float textvalue16 = Mathf.Round(float.Parse(GetDataValue(graphData[5], $"t{4}:")) / 1000);
		td20.text = textvalue16.ToString();
		if (textvalue15 < textvalue16)
		{
			ct8.color = Color.red;;
		}

		float textvalue17 = Mathf.Round(float.Parse(GetDataValue(graphData[6], $"t{1}:")) / 1000);   //screen2
		td21.text = textvalue17.ToString();
		float textvalue18 = Mathf.Round(float.Parse(GetDataValue(graphData[6], $"t{2}:")) / 1000);
		td22.text = textvalue18.ToString();
		if (textvalue17 > textvalue18)
		{
			ct9.color = Color.red;
		}

		float textvalue19 = Mathf.Round(float.Parse(GetDataValue(graphData[6], $"t{3}:")) / 1000);
		td23.text = textvalue19.ToString();
		float textvalue20 = Mathf.Round(float.Parse(GetDataValue(graphData[6], $"t{4}:")) / 1000);
		td24.text = textvalue20.ToString();
		if (textvalue19 > textvalue20)
		{
			ct10.color = Color.red;;
		}

		float textvalue21 = Mathf.Round(float.Parse(GetDataValue(graphData[7], $"t{1}:")) / 1000);
		td25.text = textvalue21.ToString();
		float textvalue22 = Mathf.Round(float.Parse(GetDataValue(graphData[7], $"t{2}:")) / 1000);
		td26.text = textvalue22.ToString();
		if (textvalue21 < textvalue22)
		{
			ct11.color = Color.red;
		}

		float textvalue23 = Mathf.Round(float.Parse(GetDataValue(graphData[7], $"t{3}:")) / 1000);
		td27.text = textvalue23.ToString();
		float textvalue24 = Mathf.Round(float.Parse(GetDataValue(graphData[7], $"t{4}:")) / 1000);
		td28.text = textvalue24.ToString();
		if (textvalue23 < textvalue24)
		{
			ct12.color = Color.red;
		}

		string tv10 = (GetDataValue(graphData[8], $"t{1}:"));
		tv10 = tv10.Substring(0, tv10.Length - 1);
		float textvalue25 = Mathf.Round(float.Parse(tv10));
		td29.text = GetDataValue(graphData[8], $"t{1}:");
		string tv11 = (GetDataValue(graphData[8], $"t{2}:"));
		tv11 = tv11.Substring(0, tv11.Length - 1);
		float textvalue26 = Mathf.Round(float.Parse(tv11));
		td30.text = GetDataValue(graphData[8], $"t{2}:");
		if (textvalue25 > textvalue26)
		{
			ct13.color = Color.red;

		}

		string tv12 = (GetDataValue(graphData[8], $"t{3}:"));
		tv12 = tv12.Substring(0, tv12.Length - 1);
		float textvalue27 = float.Parse(tv12);
		td31.text = GetDataValue(graphData[8], $"t{3}:");
		string tv13 = (GetDataValue(graphData[8], $"t{4}:"));
		tv13 = tv13.Substring(0, tv13.Length - 1);
		float textvalue28 = float.Parse(tv13);
		td32.text = GetDataValue(graphData[8], $"t{4}:");
		if (textvalue27 > textvalue28)
		{
			ct14.color = Color.red;;
		}

		float textvalue29 = float.Parse(GetDataValue(graphData[9], $"t{1}:")) / 1000;
		td33.text = textvalue29.ToString();
		float textvalue30 = float.Parse(GetDataValue(graphData[9], $"t{2}:")) / 1000;
		td34.text = textvalue30.ToString();
		if (textvalue29 > textvalue30)
		{
			ct15.color = Color.red;
		}

		float textvalue31 = float.Parse(GetDataValue(graphData[9], $"t{3}:")) / 1000;
		td35.text = textvalue31.ToString();
		float textvalue32 = float.Parse(GetDataValue(graphData[9], $"t{4}:")) / 1000;
		td36.text = textvalue32.ToString();
		if (textvalue31 > textvalue32)
		{
			ct16.color = Color.red; 
		}

		float textvalue33 = float.Parse(GetDataValue(graphData[11], $"t{1}:")) / 1000;
		td37.text = textvalue33.ToString();
		float textvalue34 = float.Parse(GetDataValue(graphData[11], $"t{2}:")) / 1000;
		td38.text = textvalue34.ToString();
		if (textvalue33 < textvalue34)
		{
			ct17.color = Color.red;
		}

		float textvalue35 = float.Parse(GetDataValue(graphData[11], $"t{3}:")) / 1000;
		td39.text = textvalue35.ToString();
		float textvalue36 = float.Parse(GetDataValue(graphData[11], $"t{4}:")) / 1000;
		td40.text = textvalue36.ToString();
		if (textvalue35 < textvalue36)
		{
			ct18.color = Color.red;
		}

		float textvalue37 = float.Parse(GetDataValue(graphData[12], $"t{1}:")) / 1000;
		td41.text = textvalue37.ToString();
		float textvalue38 = float.Parse(GetDataValue(graphData[12], $"t{2}:")) / 1000;
		td42.text = textvalue38.ToString();
		if (textvalue37 < textvalue38)
		{
			ct19.color = Color.red;
		}

		float textvalue39 = float.Parse(GetDataValue(graphData[12], $"t{3}:")) / 1000;
		td43.text = textvalue39.ToString();
		float textvalue40 = float.Parse(GetDataValue(graphData[12], $"t{4}:")) / 1000;
		td44.text = textvalue40.ToString();
		if (textvalue39 < textvalue40)
		{
			ct20.color = Color.red;
		}

		float textvalue45 = float.Parse(GetDataValue(graphData[13], $"t{1}:")) / 1000;
		td45.text = textvalue45.ToString();
		float textvalue46 = float.Parse(GetDataValue(graphData[13], $"t{2}:")) / 1000;
		td46.text = textvalue46.ToString();
		if (textvalue45 > textvalue46)
		{
			ct21.color = Color.red;
		}

		float textvalue47 = float.Parse(GetDataValue(graphData[13], $"t{3}:")) / 1000;
		td48.text = textvalue47.ToString();
		float textvalue48 = float.Parse(GetDataValue(graphData[13], $"t{4}:")) / 1000;
		td49.text = textvalue48.ToString();
		if (textvalue47 > textvalue48)
		{
			ct22.color = Color.red;
		}

		float textvalue49 = float.Parse(GetDataValue(graphData[14], $"t{1}:")) / 1000;
		td50.text = textvalue49.ToString();
		float textvalue50 = float.Parse(GetDataValue(graphData[14], $"t{2}:")) / 1000;
		td51.text = textvalue50.ToString();
		if (textvalue49 < textvalue50)
		{
			ct23.color = Color.red;
		}

		float textvalue51 = float.Parse(GetDataValue(graphData[14], $"t{3}:")) / 1000;
		td52.text = textvalue51.ToString();
		float textvalue52 = float.Parse(GetDataValue(graphData[14], $"t{4}:")) / 1000;
		td53.text = textvalue52.ToString();
		if (textvalue51 < textvalue52)
		{
			ct24.color = Color.red;
		}

		float textvalue53 = float.Parse(GetDataValue(graphData[15], $"t{1}:")) / 1000;
		td54.text = textvalue53.ToString();
		float textvalue54 = float.Parse(GetDataValue(graphData[15], $"t{2}:")) / 1000;
		td55.text = textvalue54.ToString();
		if (textvalue53 > textvalue54)
		{
			ct25.color = Color.red;
		}

		float textvalue55 = float.Parse(GetDataValue(graphData[15], $"t{3}:")) / 1000;
		td56.text = textvalue55.ToString();
		float textvalue56 = float.Parse(GetDataValue(graphData[15], $"t{4}:")) / 1000;
		td57.text = textvalue56.ToString();
		if (textvalue55 > textvalue56)
		{
			ct26.color = Color.red;;
		}

		chart.DataSource.EndBatch();
	}

	string GetDataValue(string data, string index)
	{
		string value = data.Substring(data.IndexOf(index, StringComparison.CurrentCulture) + index.Length);
		if (value.Contains("|")) value = value.Remove(value.IndexOf("|", StringComparison.CurrentCulture));
		return value;
	}

}
