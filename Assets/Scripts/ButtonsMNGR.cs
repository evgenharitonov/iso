﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using TMPro;

public class ButtonsMNGR : MonoBehaviour
{

	public TMP_InputField nameField;
	public TMP_InputField passwordField;
	public Button submitButton;
	public GameObject logerror, loginp, passinp, togg, button1, button2, button3;
	public string WWWRegisterManager, WWWLoadManager;
	public string[] login, monthcheck;
	public Toggle remember, cancellogin;

	public void Start()
	{
		StartCoroutine(CurrentMonthCheck());
		if (PlayerPrefs.GetString("user") == "OK")
		{
			loginp.SetActive(false);
			passinp.SetActive(false);
			togg.SetActive(false);
			button1.SetActive(false);
			button2.SetActive(true);
			button3.SetActive(true);
			// PlayerPrefs.DeleteAll();
		}
	}

	public void UserSwitch()
	{
		loginp.SetActive(true);
		passinp.SetActive(true);
		togg.SetActive(true);
		button1.SetActive(true);
		button2.SetActive(false);
		button3.SetActive(false);
		PlayerPrefs.DeleteAll();
	}

	public IEnumerator CurrentMonthCheck()
	{
		WWWForm table = new WWWForm();
		table.AddField("tablename", "Users");
		WWW dataDB = new WWW(WWWLoadManager, table);
		yield return dataDB;
		string msg = dataDB.text;
		monthcheck = msg.Split('~');
	}

	public void Usersaved()
	{

		if (PlayerPrefs.GetString("Dep1") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 1);
		}
		else if (PlayerPrefs.GetString("Dep2") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 2);
		}
		else if (PlayerPrefs.GetString("Dep3") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 3);
		}
		else if (PlayerPrefs.GetString("Dep4") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 4);
		}
		else if (PlayerPrefs.GetString("Dep5") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 5);
		}
		else if (PlayerPrefs.GetString("Dep6") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 6);
		}
		else if (PlayerPrefs.GetString("Dep7") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 7);
		}
		else if (PlayerPrefs.GetString("Dep8") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 8);
		}
		else if (PlayerPrefs.GetString("Dep9") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 9);
		}
		else if (PlayerPrefs.GetString("Dep10") == "OK")
		{
			SceneManager.LoadScene(sceneBuildIndex: 10);
		}

	}


	public void Login()
	{
		StartCoroutine(Register());
	}

	public void VerifyInputs()
	{
		submitButton.interactable = (nameField.text.Length >= 8 && passwordField.text.Length >= 8);
	}

	IEnumerator Register()
	{
		if (PlayerPrefs.HasKey("uname") && PlayerPrefs.HasKey("psword"))
		{
			WWWForm table = new WWWForm();
			table.AddField("uname", PlayerPrefs.GetString("uname"));
			table.AddField("password", PlayerPrefs.GetString("psword"));
			WWW dataDB = new WWW(WWWRegisterManager, table);
			yield return dataDB;
			string msg = dataDB.text;
			login = msg.Split('~');
			if (login[0] != "Wrong username/password combination")
			{
				PlayerPrefs.SetString("currentmonth", GetDataValue(monthcheck[0], "t12:"));
				for (int i = 2; i < 12; i++)
				{
					if (GetDataValue(login[0], $"t{i}:") == "ДА")
					{
						PlayerPrefs.SetString($"Dep{i - 1}", "OK");
					}
					else
					{
						PlayerPrefs.SetString($"Dep{i - 1}", "NO");
					}
				}

				if (PlayerPrefs.GetString("Dep1") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 1);
				}
				else if (PlayerPrefs.GetString("Dep2") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 2);
				}
				else if (PlayerPrefs.GetString("Dep3") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 3);
				}
				else if (PlayerPrefs.GetString("Dep4") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 4);
				}
				else if (PlayerPrefs.GetString("Dep5") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 5);
				}
				else if (PlayerPrefs.GetString("Dep6") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 6);
				}
				else if (PlayerPrefs.GetString("Dep7") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 7);
				}
				else if (PlayerPrefs.GetString("Dep8") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 8);
				}
				else if (PlayerPrefs.GetString("Dep9") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 9);
				}
				else if (PlayerPrefs.GetString("Dep10") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 10);
				}

			}
		}
		else
		{
			WWWForm table = new WWWForm();
			table.AddField("uname", nameField.text);
			table.AddField("password", passwordField.text);
			WWW dataDB = new WWW(WWWRegisterManager, table);
			yield return dataDB;
			string msg = dataDB.text;
			login = msg.Split('~');
			if (login[0] != "Wrong username/password combination")
			{
				PlayerPrefs.SetString("currentmonth", GetDataValue(monthcheck[0], "t12:"));
				if (remember.isOn)
				{
					PlayerPrefs.SetString("user", "OK");
					PlayerPrefs.SetString("uname", nameField.text);
					PlayerPrefs.SetString("psword", passwordField.text);
				}
				for (int i = 2; i < 12; i++)
				{
					if (GetDataValue(login[0], $"t{i}:") == "ДА")
					{
						PlayerPrefs.SetString($"Dep{i - 1}", "OK");
					}
					else
					{
						PlayerPrefs.SetString($"Dep{i - 1}", "NO");
					}

				}

				if (PlayerPrefs.GetString("Dep1") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 1);
				}
				else if (PlayerPrefs.GetString("Dep2") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 2);
				}
				else if (PlayerPrefs.GetString("Dep3") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 3);
				}
				else if (PlayerPrefs.GetString("Dep4") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 4);
				}
				else if (PlayerPrefs.GetString("Dep5") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 5);
				}
				else if (PlayerPrefs.GetString("Dep6") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 6);
				}
				else if (PlayerPrefs.GetString("Dep7") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 7);
				}
				else if (PlayerPrefs.GetString("Dep8") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 8);
				}
				else if (PlayerPrefs.GetString("Dep9") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 9);
				}
				else if (PlayerPrefs.GetString("Dep10") == "OK")
				{
					SceneManager.LoadScene(sceneBuildIndex: 10);
				}
			}
			else
			{
				logerror.SetActive(true);
			}
		}
	}


	public void Quit()
	{
		Application.Quit();
	}

	public string GetDataValue(string data, string index)
	{
		string value = data.Substring(data.IndexOf(index, StringComparison.CurrentCulture) + index.Length);
		if (value.Contains("|")) value = value.Remove(value.IndexOf("|", StringComparison.CurrentCulture));
		return value;
	}
}
