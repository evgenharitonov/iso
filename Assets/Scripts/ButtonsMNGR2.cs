﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsMNGR2 : MonoBehaviour
{
	public Button but1, but2, but3, but4, but5, but6, but7, but8, but9, but10;

	public void Start()
	{
		if (PlayerPrefs.GetString("Dep1") == "OK")
		{
			but1.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep2") == "OK")
		{
			but2.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep3") == "OK")
		{
			but3.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep4") == "OK")
		{
			but4.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep5") == "OK")
		{
			but5.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep6") == "OK")
		{
			but6.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep7") == "OK")
		{
			but7.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep8") == "OK")
		{
			but8.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep9") == "OK")
		{
			but9.interactable = true;
		}
		if (PlayerPrefs.GetString("Dep10") == "OK")
		{
			but10.interactable = true;
		}


	}

	public void Dep1()
	{
		SceneManager.LoadScene(sceneBuildIndex: 1);
	}

	public void Dep2()
	{
		SceneManager.LoadScene(sceneBuildIndex: 2);
	}

	public void Dep3()
	{
		SceneManager.LoadScene(sceneBuildIndex: 3);
	}

	public void Dep4()
	{
		SceneManager.LoadScene(sceneBuildIndex: 4);
	}

	public void Dep5()
	{
		SceneManager.LoadScene(sceneBuildIndex: 5);
	}

	public void Dep6()
	{
		SceneManager.LoadScene(sceneBuildIndex: 6);
	}

	public void Dep7()
	{
		SceneManager.LoadScene(sceneBuildIndex: 7);
	}

	public void Dep8()
	{
		SceneManager.LoadScene(sceneBuildIndex: 8);
	}

	public void Dep9()
	{
		SceneManager.LoadScene(sceneBuildIndex: 9);
	}

	public void Dep10()
	{
		SceneManager.LoadScene(sceneBuildIndex: 10);
	}

	public void Quit()
	{
		//Application.Quit();
		System.Diagnostics.Process.GetCurrentProcess().Kill();
	}
}
