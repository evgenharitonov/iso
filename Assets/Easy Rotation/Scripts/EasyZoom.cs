﻿using UnityEngine;

public class EasyZoom : MonoBehaviour
{
    public float Speed = 0.5f; // The rate of change of the field of view 
    float distance; // The distance between the different touches
    float distance_prev; // The distance stored for the previous frame
    public Camera cam; //this is the selected camera
    bool beginTouch = false; // 


    void Update()
    {

        //WHen the user presses the screen, we must reset the distances
        if (Input.touchCount == 1)
        {
            beginTouch = true;
        }

        // First we need to impose the condition of the number of touches equal to 2
        if (Input.touchCount == 2)
        {

            // Save the position of both touches
            Touch touch_0 = Input.GetTouch(0);
            Touch touch_1 = Input.GetTouch(1);



            // the previous distance is the one obtained in the last frame
            if (beginTouch)
            {
                distance_prev = (touch_1.position - touch_0.position).magnitude;
                beginTouch = false;
            }
            else
            {
                distance_prev = distance;
            }
                       

            // Obtain the distance between both touches 0 ---- 1 using the magnitude of the vector
            distance = (touch_1.position - touch_0.position).magnitude;

            // The value of the difference of distance in each frame is:
            float distance_delta = distance - distance_prev;

            // parameters in camera depend of the type of camera then,
            if (cam.orthographic)
            {
                //it should be above zero
                cam.orthographicSize -= distance_delta * Speed;
                cam.orthographicSize = Mathf.Max(cam.orthographicSize, 0.01f);
            }
            else
            {
                // it should be between 0 and 180
                cam.fieldOfView -= distance_delta * Speed;
                cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, 0.01f, 179.99f);
            }


        }
    }
}