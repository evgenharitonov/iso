﻿using System.Collections;
using UnityEngine;
using System;
using ChartAndGraph;
using TMPro;
using UnityEngine.UI;

public class GraphBuilder : MonoBehaviour
{

	public string[] graphData;
	public string WWWLoadManager;
	public string tablename;
	public BarChart chart, chart2, chart3;
	public TMP_Text motnhaj, motnhaj1, motnhaj2, motnhaj3, motnhaj4, motnhaj5, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, value13, value14, value15, value16, value17, value18, value19, value20, value21, value22, value23, value24, value25, value26, value27;
	public RawImage coltrigger, coltrigger2, coltrigger3, coltrigger4, coltrigger5, coltrigger6, coltrigger7, coltrigger8, coltrigger9;
	public int monthajusted, monthajusted2, monthajusted3;
	public string material1, material2, material3, material4, material5, material6;

	void Start()
	{
		StartCoroutine(GraphBuild());
	}

	IEnumerator GraphBuild()
	{
		WWWForm table = new WWWForm();
		table.AddField("tablename", tablename);
		WWW dataDB = new WWW(WWWLoadManager, table);
		yield return dataDB;
		string msg = dataDB.text;
		graphData = msg.Split('~');
		float tmpData = 0;
		float tmpData2 = 0;
		float tmpData3 = 0;
		float tmpData4 = 0;
		float tmpData5 = 0;
		float tmpData6 = 0;
		float tmpData7 = 0;
		float tmpData8 = 0;
		float tmpData9 = 0;
		float tmpData10 = 0;
		float tmpData11 = 0;
		float tmpData12 = 0;
		float tmpData13 = 0;
		float tmpData14 = 0;
		float tmpData15 = 0;
		float tmpData16 = 0;
		float tmpData17 = 0;
		float tmpData18 = 0;
		float tmpData19 = 0;
		float tmpData20 = 0;
		float tmpData21 = 0;
		float tmpData22 = 0;
		float tmpData23 = 0;
		float tmpData24 = 0;
		float tmpData25 = 0;
		float tmpData26 = 0;
		float tmpData27 = 0;

		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{1}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{1}:")));
			tmpData = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{2}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{2}:")));
			tmpData2 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData3 = tmpData2 - tmpData;
		if (tmpData3 < 0 && tmpData3 != 0)
		{
			coltrigger.color = Color.red;
		}
		else
		{
			coltrigger.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{3}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{3}:")));
			tmpData4 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{4}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{4}:")));
			tmpData5 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData6 = tmpData5 - tmpData4;
		if (tmpData6 < 0 && tmpData6 != 0)
		{
			coltrigger2.color = Color.red;
		}
		else
		{
			coltrigger2.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{6}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{6}:")));
			tmpData7 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[4], $"t{7}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[4], $"t{7}:")));
			tmpData8 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData9 = tmpData8 - tmpData7;
		if (tmpData9 < 0 && tmpData9 != 0)
		{
			coltrigger3.color = Color.red;
		}
		else
		{
			coltrigger3.color = Color.green;
		}

		value1.text = tmpData.ToString();
		value2.text = tmpData2.ToString();
		value3.text = tmpData3.ToString();
		value4.text = tmpData4.ToString();
		value5.text = tmpData5.ToString();
		value6.text = tmpData6.ToString();
		value7.text = tmpData7.ToString();
		value8.text = tmpData8.ToString();
		value9.text = tmpData9.ToString();

		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{1}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{1}:")));
			tmpData10 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{2}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{2}:")));
			tmpData11 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData12 = tmpData11 - tmpData10;
		if (tmpData12 < 0 && tmpData12 != 0)
		{
			coltrigger4.color = Color.red;
		}
		else
		{
			coltrigger4.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{3}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{3}:")));
			tmpData13 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{4}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{4}:")));
			tmpData14 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData15 = tmpData14 - tmpData13;
		if (tmpData15 < 0 && tmpData15 != 0)
		{
			coltrigger5.color = Color.red;
		}
		else
		{
			coltrigger5.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{6}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{6}:")));
			tmpData16 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[9], $"t{7}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{7}:")));
			tmpData17 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData18 = tmpData17 - tmpData16;
		if (tmpData18 < 0 && tmpData18 != 0)
		{
			coltrigger6.color = Color.red;
		}
		else
		{
			coltrigger6.color = Color.green;
		}

		value10.text = tmpData10.ToString();
		value11.text = tmpData11.ToString();
		value12.text = tmpData12.ToString();
		value13.text = tmpData13.ToString();
		value14.text = tmpData14.ToString();
		value15.text = tmpData15.ToString();
		value16.text = tmpData16.ToString();
		value17.text = tmpData17.ToString();
		value18.text = tmpData18.ToString();

		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{1}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[14], $"t{1}:")));
			tmpData19 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{2}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[14], $"t{2}:")));
			tmpData20 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData21 = tmpData20 - tmpData19;
		if (tmpData21 < 0 && tmpData21 != 0)
		{
			coltrigger7.color = Color.red;
		}
		else
		{
			coltrigger7.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{3}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[9], $"t{3}:")));
			tmpData22 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{4}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[14], $"t{4}:")));
			tmpData23 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData24 = tmpData23 - tmpData22;
		if (tmpData24 < 0 && tmpData24 != 0)
		{
			coltrigger8.color = Color.red;
		}
		else
		{
			coltrigger8.color = Color.green;
		}

		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{6}:")) == false)
		{
			var tmpGraphValue = Mathf.Round(float.Parse(GetDataValue(graphData[14], $"t{6}:")));
			tmpData25 = Mathf.Round(tmpGraphValue / 1000);
		}
		if (String.IsNullOrEmpty(GetDataValue(graphData[14], $"t{7}:")) == false)
		{
			var tmpGraphValue2 = Mathf.Round(float.Parse(GetDataValue(graphData[14], $"t{7}:")));
			tmpData26 = Mathf.Round(tmpGraphValue2 / 1000);
		}
		tmpData27 = tmpData26 - tmpData25;
		if (tmpData27 < 0 && tmpData27 != 0)
		{
			coltrigger9.color = Color.red;
		}
		else
		{
			coltrigger9.color = Color.green;
		}

		value19.text = tmpData19.ToString();
		value20.text = tmpData20.ToString();
		value21.text = tmpData21.ToString();
		value22.text = tmpData22.ToString();
		value23.text = tmpData23.ToString();
		value24.text = tmpData24.ToString();
		value25.text = tmpData25.ToString();
		value26.text = tmpData26.ToString();
		value27.text = tmpData27.ToString();

		Material expCol = Resources.Load("expectcol", typeof(Material)) as Material;
		Material expCol1 = Resources.Load(material1, typeof(Material)) as Material;
		Material expCol2 = Resources.Load(material2, typeof(Material)) as Material;
		Material expCol3 = Resources.Load(material3, typeof(Material)) as Material;
		Material expCol4 = Resources.Load(material4, typeof(Material)) as Material;
		Material expCol5 = Resources.Load(material5, typeof(Material)) as Material;
		Material expCol6 = Resources.Load(material6, typeof(Material)) as Material;
		chart.DataSource.StartBatch();

		motnhaj.text = PlayerPrefs.GetString("currentmonth");
		motnhaj1.text = PlayerPrefs.GetString("currentmonth");
		motnhaj2.text = PlayerPrefs.GetString("currentmonth");
		motnhaj3.text = PlayerPrefs.GetString("currentmonth");
		motnhaj4.text = PlayerPrefs.GetString("currentmonth");
		motnhaj5.text = PlayerPrefs.GetString("currentmonth");

		for (int i = 1; i < 13; i++)
		{
			if (String.IsNullOrEmpty(GetDataValue(graphData[1], $"t{i}:")) == false)
			{
				float spwntimer = UnityEngine.Random.Range(0.0f, 1.3f);
				float graphValueChart1 = Mathf.Round(float.Parse(GetDataValue(graphData[1], $"t{i}:")) / 1000);
				chart.DataSource.SlideValue($"CatA{i}", "Group1", graphValueChart1, spwntimer);
				chart.DataSource.SetMaterial($"CatA{i}", expCol1);
			}

			if (String.IsNullOrEmpty(GetDataValue(graphData[2], $"t{i}:")) == false)
			{
				if (GetDataValue(graphData[2], $"t{i}:") != "0") // если ФАКТ= 0 берем прогноз перевырезать город
				{
					float spwntimer = UnityEngine.Random.Range(0.0f, 1.3f);
					float graphValueChart1 = Mathf.Round(float.Parse(GetDataValue(graphData[2], $"t{i}:")) / 1000);
					chart.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart1, spwntimer);
					chart.DataSource.SetMaterial($"CatB{i}", expCol2);
				}
				else if (String.IsNullOrEmpty(GetDataValue(graphData[3], $"t{i}:")) == false)
				{
					float spwntimer = UnityEngine.Random.Range(0.0f, 1.3f);
					float graphValueChart1 = Mathf.Round(float.Parse(GetDataValue(graphData[3], $"t{i}:")) / 1000);
					chart.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart1, spwntimer);
					chart.DataSource.SetMaterial($"CatB{i}", expCol);
				}
			}



			if (String.IsNullOrEmpty(GetDataValue(graphData[6], $"t{i}:")) == false)
			{
				float graphValueChart2 = Mathf.Round(float.Parse(GetDataValue(graphData[6], $"t{i}:")) / 1000);
				chart2.DataSource.SlideValue($"CatA{i}", "Group1", graphValueChart2, 1f);
				chart2.DataSource.SetMaterial($"CatA{i}", expCol3);
			}

			if (String.IsNullOrEmpty(GetDataValue(graphData[7], $"t{i}:")) == false)
			{
				if (GetDataValue(graphData[7], $"t{i}:") != "0")
				{
					float graphValueChart2 = Mathf.Round(float.Parse(GetDataValue(graphData[7], $"t{i}:")) / 1000);
					chart2.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart2, 1f);
					chart2.DataSource.SetMaterial($"CatB{i}", expCol4);
				}
				else if (String.IsNullOrEmpty(GetDataValue(graphData[8], $"t{i}:")) == false)
				{
					float graphValueChart2 = Mathf.Round(float.Parse(GetDataValue(graphData[8], $"t{i}:")) / 1000);
					chart2.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart2, 1f);
					chart2.DataSource.SetMaterial($"CatB{i}", expCol);
				}
			}

			if (String.IsNullOrEmpty(GetDataValue(graphData[11], $"t{i}:")) == false)
			{
				float graphValueChart3 = Mathf.Round(float.Parse(GetDataValue(graphData[11], $"t{i}:")) / 1000);
				chart3.DataSource.SlideValue($"CatA{i}", "Group1", graphValueChart3, 1f);
				chart3.DataSource.SetMaterial($"CatA{i}", expCol5);
			}

			if (String.IsNullOrEmpty(GetDataValue(graphData[12], $"t{i}:")) == false)
			{
				if (GetDataValue(graphData[12], $"t{i}:") != "0")
				{
					float graphValueChart3 = Mathf.Round(float.Parse(GetDataValue(graphData[12], $"t{i}:")) / 1000);
					chart3.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart3, 1f);
					chart3.DataSource.SetMaterial($"CatB{i}", expCol6);
				}
				else if (String.IsNullOrEmpty(GetDataValue(graphData[13], $"t{i}:")) == false)
				{
					float graphValueChart3 = Mathf.Round(float.Parse(GetDataValue(graphData[13], $"t{i}:")) / 1000);
					chart3.DataSource.SlideValue($"CatB{i}", "Group1", graphValueChart3, 1f);
					chart3.DataSource.SetMaterial($"CatB{i}", expCol);
				}
			}
		}
		chart.DataSource.EndBatch();
	}

	string GetDataValue(string data, string index)
	{
		string value = data.Substring(data.IndexOf(index, StringComparison.CurrentCulture) + index.Length);
		if (value.Contains("|")) value = value.Remove(value.IndexOf("|", StringComparison.CurrentCulture));
		return value;
	}

}


//для графиков если прогноз меньше чем план красная и наооборот по факту если факт меньше плана зеленая
/// КПИ 1 слайд факт больше плана зен
/// // 2 слайд РАБОЧИЙ капитал если факт больше плана красный, производительность если факт меньше плана зеленый
/// расходы если факт больше план то красный
/// УПЗ факт боьше плана красный
/// Сменить пользователя